﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    private bool GameIsPaused;
    private GameObject PauseScreen;

    // Use this for initialization
    void Start () {
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        
    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
            PauseButton();
    }

    public void LevelPassed()
    {
        
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
        MemoryFile PlayerMemory = new MemoryFile(Statics.CurrentLevel);
        SaveAndLoad.SaveMemory(PlayerMemory);

    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    public void SaveGame()
    {
        MemoryFile PlayerMemory = new MemoryFile(Statics.CurrentLevel);
        SaveAndLoad.SaveMemory(PlayerMemory);
    }

    public void PauseButton()
    {
        GameIsPaused = !GameIsPaused;
        if (GameIsPaused)
        {
            PauseScreen = Instantiate(Resources.Load("PauseScreen") as GameObject);
            Time.timeScale = 0;
            AudioListener.pause = true;
            GameObject ContinueButton = PauseScreen.transform.Find("Panel").gameObject.transform.Find("BContinue").gameObject;
            ContinueButton.gameObject.GetComponent<Button>().onClick.AddListener(delegate () { this.GetComponent<ControlGame>().PauseButton(); });
        }
        else if (!GameIsPaused)
        {
            Destroy(PauseScreen.gameObject);
            Time.timeScale = 1;
            AudioListener.pause = false;
        }
    }
}
