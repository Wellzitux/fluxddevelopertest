using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MemoryFile  
{
    public int CurrentLevel;

    public MemoryFile(MemoryFile PlayerData)
    {
        CurrentLevel = PlayerData.CurrentLevel;
    }

    public MemoryFile(int Level)
    {
        CurrentLevel = Level;
    }
}