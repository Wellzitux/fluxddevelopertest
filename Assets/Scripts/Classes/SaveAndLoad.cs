using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveAndLoad : MonoBehaviour
{
    public static void SaveMemory(MemoryFile PlayerMemory)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/PlayerData.save";
        FileStream stream = new FileStream(path, FileMode.Create);

        MemoryFile data = new MemoryFile(PlayerMemory);

        formatter.Serialize(stream, data);
        stream.Close();

    }

    public static MemoryFile LoadMemory()
    {
        string path = Application.persistentDataPath + "/PlayerData.save";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            MemoryFile PlayerMemory = (MemoryFile)formatter.Deserialize(stream);
            stream.Close();
            return PlayerMemory;
        }
        else
        {
            return null;
        }
    }

}
